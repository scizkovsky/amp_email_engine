export interface IEmail {
    id?: number;
    plainText?: any;
    htmlContent?: any;
    ampContent?: any;
}

export class Email implements IEmail {
    constructor(public id?: number, public plainText?: any, public htmlContent?: any, public ampContent?: any) {}
}
