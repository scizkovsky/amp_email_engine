package eu.itify.amp.service.dto;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the Email entity.
 */
public class EmailDTO implements Serializable {

    private Long id;

    @Lob
    private String plainText;

    @Lob
    private String htmlContent;

    @Lob
    private String ampContent;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getAmpContent() {
        return ampContent;
    }

    public void setAmpContent(String ampContent) {
        this.ampContent = ampContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmailDTO emailDTO = (EmailDTO) o;
        if (emailDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), emailDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmailDTO{" +
            "id=" + getId() +
            ", plainText='" + getPlainText() + "'" +
            ", htmlContent='" + getHtmlContent() + "'" +
            ", ampContent='" + getAmpContent() + "'" +
            "}";
    }
}
