package eu.itify.amp.service.dto;

public class CompleteEmailDTO {
    private String from;
    private String to;
    private String subject;
    private Long resourceId;

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "CompleteEmailDTO [from=" + from + ", to=" + to + ", subject=" + subject + ", resourceId=" + resourceId
                + "]";
    }

}
