/**
 * View Models used by Spring MVC REST controllers.
 */
package eu.itify.amp.web.rest.vm;
