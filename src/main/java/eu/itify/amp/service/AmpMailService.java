package eu.itify.amp.service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import eu.itify.amp.service.dto.EmailDTO;

@Service
public class AmpMailService {
    private final JavaMailSender javaMailSender;

    public AmpMailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendAmpEmail(String from, String to, String subject, EmailDTO emailDTO) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            mimeMessage.addFrom(InternetAddress.parse(from));

            mimeMessage.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            mimeMessage.setSubject(subject);
            mimeMessage.setReplyTo(InternetAddress.parse(from));

            // create wrapper multipart/alternative part
            MimeMultipart ma = new MimeMultipart("alternative");
            mimeMessage.setContent(ma);
            // create the plain text
            BodyPart plainText = new MimeBodyPart();
            plainText.setContent(replaceEmail(emailDTO.getPlainText(), to), "text/plain; charset=\"UTF-8\"");
            ma.addBodyPart(plainText);

            // create the amp part
            BodyPart amp = new MimeBodyPart();
            amp.setContent(replaceEmail(emailDTO.getAmpContent(), to).getBytes(StandardCharsets.UTF_8),
                    "text/x-amp-html; charset=\"UTF-8\"");

            ma.addBodyPart(amp);
            // create the html part
            BodyPart html = new MimeBodyPart();
            html.setContent(replaceEmail(emailDTO.getHtmlContent(), to), "text/html; charset=\"UTF-8\"");
            ma.addBodyPart(html);

            mimeMessage.setContent(ma);
            // send
            javaMailSender.send(mimeMessage);

        } catch (AddressException e) {
            // log.warn("AddressException '{}'", to, e);
            e.printStackTrace();
        } catch (MessagingException e) {
            // log.warn("MessagingException '{}'", to, e);
            e.printStackTrace();
        }
    }

    private static String replaceEmail(String in, String email) {
        String emailTarget = "##emailhash##";
        String emailInBase64 = Base64.getEncoder().encodeToString(email.getBytes(StandardCharsets.UTF_8));

        String replacedString = in.replaceAll(emailTarget, emailInBase64);

        return replacedString;
    }
}
