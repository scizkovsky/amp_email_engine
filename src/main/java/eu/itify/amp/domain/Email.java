package eu.itify.amp.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Email.
 */
@Entity
@Table(name = "email")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    @Column(name = "plain_text")
    private String plainText;

    @Lob
    @Column(name = "html_content")
    private String htmlContent;

    @Lob
    @Column(name = "amp_content")
    private String ampContent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlainText() {
        return plainText;
    }

    public Email plainText(String plainText) {
        this.plainText = plainText;
        return this;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public Email htmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
        return this;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getAmpContent() {
        return ampContent;
    }

    public Email ampContent(String ampContent) {
        this.ampContent = ampContent;
        return this;
    }

    public void setAmpContent(String ampContent) {
        this.ampContent = ampContent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Email email = (Email) o;
        if (email.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), email.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Email{" +
            "id=" + getId() +
            ", plainText='" + getPlainText() + "'" +
            ", htmlContent='" + getHtmlContent() + "'" +
            ", ampContent='" + getAmpContent() + "'" +
            "}";
    }
}
