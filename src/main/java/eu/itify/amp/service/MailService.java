package eu.itify.amp.service;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import eu.itify.amp.domain.User;
import io.github.jhipster.config.JHipsterProperties;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
            MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
                isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

    @Async
    public void sendAmpEmail(String from, String to, String subject) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            mimeMessage.addFrom(InternetAddress.parse(from));

            mimeMessage.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            mimeMessage.setSubject(subject);
            mimeMessage.setReplyTo(InternetAddress.parse(from));

            // create wrapper multipart/alternative part
            MimeMultipart ma = new MimeMultipart("alternative");
            mimeMessage.setContent(ma);
            // create the plain text
            BodyPart plainText = new MimeBodyPart();
            plainText.setText("Ahoj. Chtel jsem Te jenom pozdravit. \nSam");
            ma.addBodyPart(plainText);
            // create the amp part
            BodyPart amp = new MimeBodyPart();
            amp.setContent("<!doctype html>\n" + "<html amp4email>\n" + "<head>\n" + "  <meta charset=\"utf-8\">\n"
                    + "  <style amp4email-boilerplate>body{visibility:hidden}</style>\n"
                    + "  <script async src=\"https://cdn.ampproject.org/v0.js\"></script>\n" + "</head>\n" + "<body>\n"
                    + "Hello, world.\n\n" + "<br/><br/>"
                    + "You're receiving this newsletter because you selected that option when signing up for our newsletter. Changed your mind? <a href=\"https://www.itify.eu/unsubscribe?email="
                    + to + "\">Unsubscribe Instantly</a>.</p>"

                    + "</body>\n" + "</html>", "text/x-amp-html; charset=\"UTF-8\"");

            ma.addBodyPart(amp);
            // create the html part
            BodyPart html = new MimeBodyPart();
            html.setContent("<html><head></head>Ahoj. Chtel jsem Te jenom pozdravit. <br/>Sam</body></html>",
                    "text/html");
            ma.addBodyPart(html);

            mimeMessage.setContent(ma);
            // send
            javaMailSender.send(mimeMessage);

        } catch (AddressException e) {
            log.warn("AddressException '{}'", to, e);
            e.printStackTrace();
        } catch (MessagingException e) {
            log.warn("MessagingException '{}'", to, e);
            e.printStackTrace();
        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);

    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }
}
